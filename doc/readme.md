# Movit - Docker Base

## Descripción

Este proyecto, tiene como finalidad **Estandarizar** los proyectos desarrollados por **Movit Devs**.

## Tech briefing

Server: NGINX
PHP: 7.1
DB: Maria DB

Docker version 18.06.1-ce

## Instrucciones

Teniendo **docker** instalado, hay que navegar dentro del proyecto, ejemplo:

**cd dockerAlpine/**

### Construyendo el proyecto

Dentro de la carpeta(donde se encuentra el archivo **docker-compose.yml**) ejecutamos:

` docker-compose build `

* Esto solo se realiza la primera vez que el proyecto fue descargado

### Levantando el proyecto

Para levantar el contenedor ejecutamos:

` docker-compose up -d `

Si en tu navegador pudes acceder a:

Url: [http://localhost](http://localhost)

significa que el proyecto funciona correctamente :)

### Url personalizadas(Opcional)

(unix) Abre el archivo **hosts**, /etc/hosts y en la ultima linea agrega:

**127.0.0.1  app.docker mysql**

